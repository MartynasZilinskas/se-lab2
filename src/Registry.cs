using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace se_lab2_csharp
{
    class Registry
    {
        private List<string> keywords = new List<string>();
        private Dictionary<string, List<int>> files = new Dictionary<string, List<int>>();

        public int GetIndexedKeywordsCount()
        {
            return keywords.Count;
        }

        public int GetIndexedFilesCount()
        {
            return files.Count;
        }

        public void AddFile(string fileLocation, string content)
        {
            // Replace special characters
            var symbolsPattern = new Regex("[^a-zA-Z0-9% ._]");
            var htmlPattern = new Regex("<[^>]*>");
            var sanitizedContent = htmlPattern.Replace(content, " ");
            sanitizedContent = symbolsPattern.Replace(sanitizedContent, " ");
            string[] localKeywords = sanitizedContent
                                        .Split(' ')
                                        .Select(x => x.ToLower())
                                        .Where(x => x.Length != 0)
                                        .ToArray();

            // Resolved global keyword indexes from {files}
            List<int> globalKeywordIndexes = new List<int>();

            foreach (var keyword in localKeywords)
            {
                int keywordIndex = keywords.FindIndex(x => x == keyword);
                if (keywordIndex == -1)
                {
                    keywords.Add(keyword);
                    keywordIndex = keywords.FindIndex(x => x == keyword);
                }

                globalKeywordIndexes.Add(keywordIndex);
            }

            // Add to file registry file with saved global keyword ids from {files}.
            files.Add(fileLocation, globalKeywordIndexes);
            Console.WriteLine(String.Format("Indexed {0}", fileLocation));
        }

        public List<string> Query(string[] keywords)
        {
            List<string> result = new List<string>();

            // Resolve keyword indexes.
            List<int> resolvedKeywordIndexes = new List<int>();
            foreach (var keyword in keywords)
            {
                var index = this.keywords.FindIndex(x => x == keyword);
                if (index != -1)
                {
                    resolvedKeywordIndexes.Add(index);
                }
            }

            // Resolve files that have those keywords.
            foreach (var fileItem in files)
            {
                if (resolvedKeywordIndexes.All(x => fileItem.Value.Contains(x)))
                {
                    result.Add(fileItem.Key);
                }
            }

            return result;
        }
    }
}
