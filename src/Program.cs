﻿using System;
using System.IO;

namespace se_lab2_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] keywords = args;
            var registry = new Registry();

            foreach (string file in Directory.EnumerateFiles("./data", "*.html"))
            {
                string contents = File.ReadAllText(file);
                registry.AddFile(file, contents);
            }
            Console.WriteLine(String.Format("Indexed {0} keywords in {1} files.", registry.GetIndexedKeywordsCount(), registry.GetIndexedFilesCount()));

            var results = registry.Query(keywords);

            Console.WriteLine(String.Format("Found {0} files by query \"{1}\"", results.Count, String.Join(" ", keywords)));
            foreach (var item in results)
            {
                Console.WriteLine(item);
            }
        }
    }
}
